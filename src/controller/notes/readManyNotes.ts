/**
 * - route to get multiple notes by id (at the moment sync)
 */

import { DATA_API_KEY, BASE_URI,DATABASE, DATA_SOURCE } from "../../constants.ts";

const options = {
  method: "POST",
  headers: {
    "Content-Type": "application/json",
    "api-key": DATA_API_KEY 
  },
  body: ""
};

const readManyNotes = async ({
    params,
    response,
    request
  }: {
    params: { collection: string };
    response: any;
    request: any;
  }) => {
    try {
      if (!request.hasBody) {
        response.status = 400;
        response.body = {
          success: false,
          msg: "No Data",
        };
      } else {
        const body = await request.body();
        const ids = await body.value;
        const URI = `${BASE_URI}/find`;

        const notesList = [];

        for (let index = 0; index < ids.ids.length; index++) {
          const query = {
            collection: params.collection,
            database: DATABASE,
            dataSource: DATA_SOURCE,
            filter: { noteId: ids.ids[index] }
          };
          options.body = JSON.stringify(query);
          const dataResponse = await fetch(URI, options);
          const notes = await dataResponse.json();
          if (notes.documents !== null && notes.documents.length === 1) {
            notesList.push(notes.documents[0]);
          }
        }

        if (notesList.length === ids.ids.length) {
          response.status = 200;
          response.body = {
            success: true,
            count: notesList.length,
            data: notesList,
          };
        } else if((notesList.length !== ids.ids.length) && (notesList.length !== 0)) {
          response.status = 200;
          response.body = {
            success: true,
            count: notesList.length,
            message: "It seems that not all Notes have been found...",
            data: notesList,
          };
        } else {
          response.status = 404;
          response.body = {
            success: false,
            message: "Note was not found",
          };
        }
      }
    } catch (err) {
      response.status = 500;
      response.body = {
        success: false,
        msg: err.toString(),
      };
    }
  };

export { readManyNotes };