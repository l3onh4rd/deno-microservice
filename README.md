# deno_microservice
This is a possible microservice implementation with Deno &amp; MangoDB. This service manages notes.

## Warning

Please use deno version 1.22.3 or lower. Do not use v1.23.0 or higher. This is due to security issues.

## How to start the application

To start the application use the command:

deno task start

To load all dependencies use the command:

deno task loadDep
